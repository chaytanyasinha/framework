<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Provenience $provenience
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($provenience) ?>
            <legend class="capital-heading"><?= __('Edit Provenience') ?></legend>
            <?php
                echo $this->Form->control('provenience');
                echo $this->Form->control('region_id', ['options' => $regions, 'empty' => true]);
                echo $this->Form->control('geo_coordinates');
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $provenience->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $provenience->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List Proveniences'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Regions'), ['controller' => 'Regions', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Region'), ['controller' => 'Regions', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Archives'), ['controller' => 'Archives', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Archive'), ['controller' => 'Archives', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Dynasties'), ['controller' => 'Dynasties', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Dynasty'), ['controller' => 'Dynasties', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
