<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact $artifact
 */
?>
<h1>Dashboard</h1>

<div align="left">
	<ul>
		<li><a href="/admin/abbreviations">Abbreviations</a></li>
		<li><a href="/admin/agade_mails">Agade Mails</a></li>
		<li><a href="/admin/archives">Archives</a></li>
		<li><a href="/admin/articles/add">Articles</a></li>
		<li><a href="/admin/artifacts">Artifacts</a></li>
		<li><a href="/admin/artifacts_publications">Artifacts Publications links</a></li>
		<li><a href="/admin/artifactTypes">Artifact Types</a></li>
		<li><a href="/admin/authors">Authors</a></li>
		<li><a href="/admin/authors_publications">Authors Publications links</a></li>
		<li><a href="/admin/CdliTablet">CDLI tablet</a></li>
		<li><a href="/admin/cdlNotes">CdlNotes</a></li>
		<li><a href="/admin/collections">Collections</a></li>
		<li><a href="/admin/dates">Dates</a></li>
		<li><a href="/admin/dynasties">Dynasties</a></li>
		<li><a href="/admin/editors_publications">Editors Publications links</a></li>
		<li><a href="/admin/externalResources">ExternalResources</a></li>
		<li><a href="/admin/genres">Genres</a></li>
		<li><a href="/admin/languages">Languages</a></li>
		<li><a href="/admin/materialAspects">MaterialAspects</a></li>
		<li><a href="/admin/materialColors">MaterialColors</a></li>
		<li><a href="/admin/materials">Materials</a></li>
		<li><a href="/admin/periods">Periods</a></li>
		<li><a href="/admin/postings">Postings</a></li>
		<li><a href="/admin/proveniences">Proveniences</a></li>
		<li><a href="/admin/publications">Publications</a></li>
		<li><a href="/admin/regions">Regions</a></li>
		<li><a href="/admin/rulers">Rulers</a></li>
		<li><a href="/admin/signReadings">SignReadings</a></li>
		<li><a href="/admin/staff">Staff</a></li>
		<li><a href="/admin/users">Users</a></li>
	</ul>
</div>
