<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<main class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('Edit User') ?></div>

        <?= $this->Form->create('{{$user}}',['type'=>'post']) ?>
            <table class="table-bootstrap">
                <tbody>
                    <tr>
                        <th scope="row"><?= __('Username') ?></th>
                        <td><?= h($user['username']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Email') ?></th>
                        <td><?= h($user['email']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="new-password">New Password</label></th>
                        <td><?= $this->Form->control('password',['class' => 'col-12 input-background-child-md', 'label' => false, 'id'=>'new-password']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="confirm-password">Confirm New Password</label></th>
                        <td><?= $this->Form->control('password_confirm',['class' => 'col-12 input-background-child-md', 'label' => false, 'type' => 'password','id'=>'confirm-password']) ?></td>
                    </tr>
                </tbody>
            </table>
            <?= $this->Form->submit("Save",['class'=>'btn cdli-btn-blue ml-4']) ?>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('View your profile'), ['action' => 'profile'], ['class' => 'btn']) ?>
        <?= $this->Flash->render() ?>
    </div>

</main>
