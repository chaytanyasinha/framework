<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal[]|\Cake\Collection\CollectionInterface $journals
 */
?>

<h3 class="display-4 pt-3"><?= __('Cuneiform Digital Library Journals') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th>No.</th>
            <th>Author</th>
            <th>Title</th>
            <th>Date</th>
            <th>View</th>
            <th>File</th>
        </tr>
    </thead>
    <tbody align="left" class="journals-view-table">
        <?php $i=1; foreach ($cdlj as $artcile) {  ?>
            <tr>
                <td width="5%"><?= $artcile['serial']; ?></td>
                <td width="25%"><?= $artcile['authors']; ?></td>
                <td width="40%"><?= $artcile['title']; ?></td>
                <td width="15%"><?= $artcile['created']; ?></td>
                  <td width="15%"><a href="cdlj/view/<?= $artcile['article_id']; ?>/web">view</a> </td>
                <td width="15%"><a href="cdlj/view/<?= $artcile['article_id']; ?>">pdf</a> </td>
            </tr>
       <?php } ?>
    </tbody>
</table>

<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
    </ul>
</div>

