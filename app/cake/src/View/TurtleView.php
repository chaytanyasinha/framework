<?php
namespace App\View;

use Cake\View\SerializedView;

class TurtleView extends SerializedView
{
    use LinkedDataTrait;

    protected $subDir = 'turtle';

    protected $_responseType = 'ttl';

    protected function _serialize($serialize)
    {
        $data = $this->_dataToSerialize($serialize);
        $graph = $this->prepareDataExport($data);
        return $graph->serialise('turtle');
    }
}
