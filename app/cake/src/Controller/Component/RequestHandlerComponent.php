<?php
namespace App\Controller\Component;

use Cake\Controller\Component\RequestHandlerComponent as _RequestHandlerComponent;
use Cake\Event\Event;
use Cake\Http\Response;
use Cake\Routing\Router;

class RequestHandlerComponent extends _RequestHandlerComponent
{
    public function startup(Event $event)
    {
        parent::startup($event);

        $format = $this->request->getParam('format');
        if ($format) {
            $this->ext = $format;
        }
    }

    public function accepts($type = null)
    {
        if (!empty($this->ext)) {
            return [$this->ext];
        } else {
            return parent::accepts($type);
        }
    }

    public function beforeRedirect(Event $event, $parts, Response $response)
    {
        $request = $this->getController()->getRequest();
        $format = $request->getParam('format');
        if ($format) {
            $parts['format'] = $format;
            return $response->withLocation(Router::url($parts));
        }
    }
}
